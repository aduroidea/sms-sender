package broker

import (
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/collector"
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/utils"
	"sort"
)

func SortMessages(messages []collector.DBMessage) {
	sort.Slice(messages, func(i, j int) bool {
		return utils.StatusToPriority[messages[i].Message.Status] < utils.StatusToPriority[messages[j].Message.Status]
	})
}
