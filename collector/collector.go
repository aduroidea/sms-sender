package collector

import (
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/types"
	"database/sql"
	"fmt"
)

type SentResult struct {
	Message  *types.Message
	Response int
	Repo     *MessageRepo
}

type DBMessage struct {
	Message *types.Message
	Repo    *MessageRepo
}

func NewMessageRepo(conn *sql.DB) *MessageRepo {
	return &MessageRepo{
		conn:   conn,
		offset: 0,
	}
}

type MessageRepo struct {
	conn   *sql.DB
	offset int
}

func (m *MessageRepo) GetNextMessages(limit int) ([]DBMessage, error) {
	var messages []DBMessage
	rows, err := m.conn.Query("SELECT * FROM messages WHERE SENT IS NULL ORDER BY DATE LIMIT ? OFFSET ?", limit, m.offset)
	if err != nil {
		fmt.Println("Error on select", err)
		return messages, err
	}
	defer rows.Close()
	for rows.Next() {
		p := types.Message{}
		err := rows.Scan(
			&p.Id,
			&p.Status,
			&p.Text,
			&p.Date,
			&p.Sender,
			&p.Recipient,
			&p.Sent,
			&p.Delivery)
		if err != nil {
			fmt.Println("Scan error: ", err)
			continue
		}
		m := DBMessage{Message: &p, Repo: m}
		messages = append(messages, m)
	}
	nextOffset := len(messages)
	m.offset += nextOffset
	return messages, err
}

func (m *MessageRepo) UpdateMessage(sentResult SentResult) {
	if sentResult.Response == 0 {
		return
	}
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Error: ", r)
		}
	}()
	_, err := m.conn.Exec("update messages set SENT = ?  where ID = ?", sentResult.Message.Sent.Time, sentResult.Message.Id)
	if err != nil {
		fmt.Println("SQL: Update err: ", err)
	}
	m.offset -= 1
}
