package main

import (
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/broker"
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/collector"
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/sender"
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/utils"
	"fmt"
	"time"
)

var msgRep *collector.MessageRepo

func main() {
	fmt.Println("Sender started ...")
	cfg, err := utils.LoadConfiguration("config.yaml")
	if err != nil {
		fmt.Println("Open file problem: ", err)
	}
	for _, d := range cfg.Databases {
		conn, err := utils.OpenConnection(d.DB, d.Username, d.Password, d.DBName)
		if err != nil {
			fmt.Println(err)
		}
		msgRep = collector.NewMessageRepo(conn)
		utils.DbConnsArr = append(utils.DbConnsArr, msgRep)
	}
	duration := 1 * time.Second
	for _, msgRep := range utils.DbConnsArr {
		ticker := time.NewTicker(duration)
		go DataPoll(msgRep, ticker)
	}
	go sender.DeliverMessages()
	utils.UpdateDataPoll()
}

func DataPoll(msgRep *collector.MessageRepo, ticker *time.Ticker) {
	for _ = range ticker.C {
		messages, err := msgRep.GetNextMessages(20)
		if err != nil {
			fmt.Println("Error getting messages: ", err)
			continue
		}
		broker.SortMessages(messages)
		utils.Queue = append(utils.Queue, messages...)
	}
}
