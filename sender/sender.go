package sender

import (
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/collector"
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/types"
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/utils"
	"fmt"
	"net/http"
	"sync"
	"time"
)

func DeliverMessage(sortedMessages []collector.DBMessage) <-chan collector.SentResult {
	deliverMessage := make(chan collector.SentResult, 1)
	go func() {
		for _, m := range sortedMessages {
			res, err := CheckMessageDelivery(*m.Message)
			if err != nil {
				fmt.Println(err)
				continue
			}

			m := m
			m.Message.Sent.Time = time.Now()
			m.Message.Sent.Valid = true
			sentRes := collector.SentResult{
				Message:  m.Message,
				Response: res.StatusCode,
			}
			deliverMessage <- sentRes
		}
		close(deliverMessage)
	}()
	return deliverMessage
}

func CheckMessageDelivery(message types.Message) (*http.Response, error) {
	urlStr := fmt.Sprintf(
		"http://%s:&dlr-mask=31&dlr-url=http://dlr/dlr_smsgame.php?smscID=&answer=&dlr=&to=%s&from=%s&ts=&smsid=%d&kannel_id=",
		utils.AppConfig.URL,
		message.Recipient,
		message.Sender,
		message.Id)
	resp, err := http.Get(urlStr)
	if err != nil {
		return resp, err
	}
	defer resp.Body.Close()
	for {
		bs := make([]byte, 1014)
		n, err := resp.Body.Read(bs)
		if n == 0 || err != nil {
			break
		}
	}
	return resp, err
}

func DeliverMessages() {
	for _ = range utils.SenderTicker.C {
		var mutex sync.Mutex
		for _, m := range utils.Queue {
			if len(utils.Queue) > 0 {
				go func(m collector.DBMessage) {
					mutex.Lock()
					res, err := CheckMessageDelivery(*m.Message)
					if err != nil {
						fmt.Println(err)
					}
					sentRes := collector.SentResult{
						Message:  m.Message,
						Response: res.StatusCode,
						Repo:     m.Repo,
					}
					utils.SendedQueue = append(utils.SendedQueue, sentRes)
					mutex.Unlock()
				}(m)
				utils.Queue = utils.Queue[1:]
			}
		}
	}
}
