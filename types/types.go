package types

import (
	"database/sql"
	"time"
)

type Message struct {
	Id        int
	Status    int
	Text      string
	Date      time.Time
	Sender    string
	Recipient string
	Sent      sql.NullTime
	Delivery  sql.NullTime
}
