package utils

import (
	"bitbucket.org/Aliaksei_Putsykovich/sms-sender/collector"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/yaml.v2"
	"os"
	"time"
)

var StatusToPriority map[int]int
var AppConfig Config
var DbConnsArr []*collector.MessageRepo
var SenderTicker *time.Ticker
var CollectorTicker *time.Ticker
var Queue []collector.DBMessage
var SendedQueue []collector.SentResult

type Config struct {
	Username   string     `yaml:"Username"`
	Password   string     `yaml:"Password"`
	URL        string     `yaml:"URL"`
	DriverName string     `yaml:"DB"`
	Databases  []Database `yaml:"Databases"`
}

type Sender struct {
	Status   int    `yaml:"Status"`
	Sender   string `yaml:"Sender"`
	Priority string `yaml:"Priority"`
}
type Database struct {
	Username string   `yaml:"Username"`
	Password string   `yaml:"Password"`
	DB       string   `yaml:"DB"`
	DBName   string   `yaml:"DBName"`
	Senders  []Sender `yaml:"Senders"`
}

func OpenConnection(driverName, username, password, table string) (*sql.DB, error) {
	dataSourceName := fmt.Sprintf("%s:%s@/%s?parseTime=true", username, password, table)
	db, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		fmt.Println(" Could not open database", err)
		return db, err
	}
	return db, err
}

func LoadConfiguration(file string) (*Config, error) {
	CollectorTicker = time.NewTicker(1 * time.Millisecond)
	SenderTicker = time.NewTicker(1 * time.Second)
	Queue = []collector.DBMessage{}
	SendedQueue = []collector.SentResult{}
	f, err := os.Open(file)
	if err != nil {
		fmt.Println(err)
	}
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&AppConfig)
	if err != nil {
		return &AppConfig, err
	}
	DbConnsArr = []*collector.MessageRepo{}
	//loadStatusToPriorityMap(AppConfig.Databases[0].Senders)
	return &AppConfig, err
}

func loadStatusToPriorityMap(senders []Sender) {
	StatusToPriority = map[int]int{}
	for _, sender := range senders {
		var priority int
		switch sender.Priority {
		case "High":
			priority = 3
		case "Medium":
			priority = 2
		case "Low":
			priority = 1
		default:
			priority = 0
		}
		StatusToPriority[sender.Status] = priority
		fmt.Println(sender)
	}
}

func UpdateDataPoll() {
	for _ = range CollectorTicker.C {
		for _, result := range SendedQueue {
			result.Repo.UpdateMessage(result)
			SendedQueue = SendedQueue[1:]
		}
	}
}
